<#if shadowjar>
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
</#if>
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.net.URI

group = "${group}"
version = "${version}"

val kotlinVersion = "1.3.61"

plugins {
    java
    kotlin("jvm") version "1.3.61"
    <#if shadowjar>
        id("com.github.johnrengelman.shadow") version "5.0.0"
    </#if>
    <#if bintray>
        `maven-publish`
        id("com.jfrog.bintray") version "1.8.4"
    </#if>
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.freeCompilerArgs = listOf("-Xjvm-default=compatibility")
}

repositories {
    mavenCentral()
    maven { url = URI.create("https://hub.spigotmc.org/nexus/content/repositories/snapshots") }
    maven { url = URI.create("https://oss.sonatype.org/content/repositories/snapshots/") }
}

dependencies {
    compileOnly(kotlin("stdlib-jdk8", kotlinVersion))
    compileOnly("dev.reactant:reactant:0.1.5")
    compileOnly("org.spigotmc:spigot-api:1.15.1-R0.1-SNAPSHOT")

    /** The external library you would like to use */
    /** implementation("...")    */
}


val sourcesJar by tasks.registering(Jar::class) {
    dependsOn(JavaPlugin.CLASSES_TASK_NAME)
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

<#if shadowjar>
    val shadowJar = (tasks["shadowJar"] as ShadowJar).apply {
        //relocate("org.from.package", "org.target.package")
    }
</#if>

<#if autodeploy>
    val deployPlugin by tasks.registering(Copy::class) {
        dependsOn(shadowJar)
        System.getenv("PLUGIN_DEPLOY_PATH")?.let {
            from(shadowJar)
            into(it)
        }
    }
</#if>

val build = (tasks["build"] as Task).apply {
    arrayOf(
            sourcesJar
            <#if shadowjar>
                , shadowJar
            </#if>
            <#if autodeploy>
                , deployPlugin
            </#if>
    ).forEach { dependsOn(it) }
}

<#if bintray>
    publishing {
        publications {
            create<MavenPublication>("maven") {
                from(components["java"])
                artifact(sourcesJar.get())
                artifact(shadowJar)

                groupId = group.toString()
                artifactId = project.name
                version = version
            }
        }
    }

    bintray {
        user = System.getenv("BINTRAY_USER")
        key = System.getenv("BINTRAY_KEY")
        setPublications("maven")
        publish = true
        override = true
        pkg.apply {
            repo = "${bintrayRepo}"
            name = project.name
            setLicenses("GPL-3.0")
            vcsUrl = "${vcsUrl}"
        }
    }
</#if>
